/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codingbatlogic1;

/**
 *
 *Autora: Irene Benito 
 */
public class CodingBatLogic1IreneBenito {
public boolean cigarParty(int cigars, boolean isWeekend) {

/*
 * vamos ha hacer una fiesta de ardillas, asi que analizamos el numero 
 *de bellotas y si es finde o no
 */


		if(cigars > 39 && cigars < 61) {
			return true; //la fiesta es un existo por que el numero de bellotas es el correcto 
		}
		else if(isWeekend && cigars > 60) {
			return true; //la fiesta es un existo por que es fin de semana 
		}
		else {
			return false; //la fiesta es un mojón 
		}

}
   public int dateFashion(int you, int date) {
      // uno menor-igual que dos y otro mayor-igual que 8, da 0
      if (you<=2 && date>=8) {
        return 0; 
      }
      if (you>=8 && date<=2) {
        return 0;
      }

      //algun mayor o igual que ocho , da un 2 
      if (you>=8 || date>=8){
        return 2; 
      }

      // algun menor o igual que dos , da un 0
      else if (you<=2 || date<=2){
        return 0;
      }

     //sino 1
      else {
        return 1;
      }

}

   public boolean squirrelPlay(int temp, boolean isSummer) {
  if (isSummer && temp<=100 && temp>=60){
    return true ; //verano entre 100 y 60 
  }
  else if(!isSummer && temp<=90 && temp>=60){
    return true ; //no verano entre 60 y 90 
  } 
  else{
    return false; //sino , falso 
  }
}
   public int caughtSpeeding(int speed, boolean isBirthday) {
  if(isBirthday) {
			speed = speed - 5;//si es su cumple , se permiten 5 mas de vel
		}
		if(speed<=60) {
			return 0;//menos de 60 inclusive no hay multa
		}
		if(speed>=61 && speed<=80) {
			return 1;//si es entre 61/80 inclusive es leve 
		}
		else {
			return 2;//mas de 81 inclusive es grave
		}
}

   public int sortaSum(int a, int b) {
  if(a+b <=19 && a+b >=10){
    return 20 ; // suma entre 10 y 19 inclusive , da 20
  }
  else if (a+b >=19 && a+b <=10){
    return a+b ;//suma menor de 10 o mayor de 20 , da la suma 
  }
  return a+b; //sino , la suma 
}

   public String alarmClock(int day, boolean vacation) {
  
  if (vacation && (day>=6 || day<=0)){
    return "off" ; //finde en vacas 
  }
  
  if (vacation && (day<=5 && day>0)){
    return "10:00" ;// diario en vacas 
  }

  if (!vacation && (day<=5 && day>0)){
    return "7:00" ;// diario sin vacas 
  }
  if (!vacation && (day>=6 || day<=0)){
    return "10:00" ;// finde sin vacas 
  }
  return "Hola fraxito" ; //comentario aleatorio 
}

   public boolean love6(int a, int b) {
  if (a == 6) {
    return true ; //si a es 6
  }
   if (b == 6) {
    return true ;//si b es 6
  }
  if ((a+b) == 6 ){ //si la suma es 6
    return true;
  }
  //si la resta es 6
   if ((a-b) == 6 ){
    return true;
  }
   if ((b-a) == 6 ){
    return true;
  }
  //sino ,falsito
  return false;
}

   public boolean in1To10(int n, boolean outsideMode) {
  if (!outsideMode && (n <=10 && n>=1)){
    return true ; // entre 1-20 inclusive sin outside es true 
  }
  if (outsideMode && (n >=10 || n<=1)){
    return true ; //mayor que 10 menor que 1 inclusive es true 
  }
  return false ; // sino es falso 
}

   public boolean specialEleven(int n) {
  if ((n % 11) == 0){
    return true ; // si el resto es 0 , es multiplo
  }
  if ((n % 11) == 1){
    return true ; //si el resto es 1 , es un numero mayor del multiplo
  }
  return false;// sino , falsete
}

   public boolean more20(int n) {
   if ((n % 20) == 2){
    return true ; //si el resto es dos 
  }
  if ((n % 20) == 1){
    return true ; // si el resto es uno 
  }
  return false; //sino , nonono
}

   public boolean old35(int n) {
   if ((n % 3) == 0 && (n % 5) == 0){
    return false ; //si es multiplo de los dos 
  }
   if ((n % 3) == 0 || (n % 5) == 0){
    return true ; //si es multiplo de uno solo
  }
  return false; //sino , negativo
}

public boolean less20(int n) {
   if ((n % 20) == 18){
    return true ; //si es dos menos del multiplo
  }
   if ((n % 20) == 19 ){
    return true ; //si es uno menos del multiplo
  }
  return false; //sino , next
}

public boolean nearTen(int num) {
   if ((num % 10) == 8 ){
    return true ; //si es dos menos del multiplo
  }
   if ((num % 10) == 9 ){
    return true ; //si es uno menos del multiplo
  }
   if ((num % 10) == 2 ){
    return true ; //si es dos mas del multiplo
  }
   if ((num % 10) == 1 ){
    return true ; //si es uno menos del multiplo
  }
   if ((num % 10) == 0 ){
    return true ; //si es  multiplo
  }
  return false; //sino , casi pero no
}

public int teenSum(int a, int b) {
  if (a<=19 && a>=13) {
    return 19; //si a esta entre 13 y 19 
  }
   if (b<=19 && b>=13) {
    return 19;//si b esta entre 13 y 19 
  }
 return a+b ; //sino , sumita al canto 
}

public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
    if (isAsleep) {
			return false; //si duerme , duerme 
		}
		if(isMorning && isMom) {
			return true; //solo se lo coge a su madre 
		}
		if (isMorning) {
			return false; //esta to sopa
		}
		return true;
	}

public String fizzString(String str) {
  //creo un string uno 
  String one = str.substring(0,1); 
  //creo un string dos
  String two = str.substring(str.length()-1,str.length());
  if ( one.equals ("f") && two.equals ("b")){
    return "FizzBuzz"; //si se cumplen las dos 
  }
  if ( one.equals ("f")){
    return "Fizz";//si empieza str por f 
  }
   if ( two.equals ("b")){
    return "Buzz";//si acaba str en b
  }
  //sino , devolvemos el str
  return str;
}

public String fizzString2(int n) {
  if ((n % 3) == 0 && (n % 5) == 0){
    return "FizzBuzz!" ; //si es multiplo de los dos 
  }
   if ((n % 3) == 0){
    return "Fizz!" ; //si es multiplo de 3
  }
   if ((n % 5) == 0){
    return "Buzz!" ; //si es multiplo de 5
  }
  return n + "!"; //sino , pues el resultado con exclamacion
}

public boolean twoAsOne(int a, int b, int c) {
  //creo innecesario comentar esto la verdad 
  if ( a + b == c ){
    return true ;
  }
  if (a + c == b ){
    return true ;
  } 
  if (c + b == a ){
    return true ;
  }
  return false;
}

public boolean inOrder(int a, int b, int c, boolean bOk) {
  //me empieza a cansar comentar cosas tan obvias 
  if (bOk && c>b){
    return true;
  }
  if(b>a && c>b){
  return true ;
  }
  return false;
}

public int teaParty(int tea, int candy) {
  
  if(tea <5 || candy < 5){
  return 0;//alguno menor que 5 
  }
  if (tea >= (candy*2)){
    return 2; // te mas del doble
  }
  if (candy >= (tea*2)){
    return 2;//dulce mas del doble 
  }
  if (tea >= 5 && candy >= 5 ){
    return 1;// los dos mayores-iguales que 5
  }
  
  return 9;//numero aleatorio
}

public boolean inOrderEqual(int a, int b, int c, boolean equalOk) {
  //aqui sigo comentando los ejercicios
  if (a<b && b<c ){
    return true ;//de menor a mayor 
  }
  if (equalOk && a==b && b<c){
    return true ;//dos primeros iguales 
  }
  if (equalOk && a<b && b==c){
    return true ;//dos ultimos iguales
  }
  if (equalOk && a==b && b==c){
    return true ;//los tres iguales
  }
  return false;// por si lo demas no se cumple
}

public boolean lastDigit(int a, int b, int c) {
  if (a%10 == b%10){
    return true ; //resto a es igual a resto de b
  }
  if (a%10 == c%10){
    return true ;//resto a es igual a resto de c
  }
  if (c%10 == b%10){
    return true ;//resto c es igual a resto de b
  }
  return false ; // lo demas no se cumple
}

public boolean lessBy10(int a, int b, int c) {
  if (a-10 >= b || a-10 >= c){
    return true ;//comparamos a 
  }
  if (b-10 >= a || b-10 >= c){
    return true ;//comparamos b 
  }
  if (c-10 >= b || c-10 >= a){
    return true ;//comparamos c
  }
  return false;//los restos 
}

public int withoutDoubles(int die1, int die2, boolean noDoubles) {
  //ya solo quedan 7 ejercicios, no me lo creo
  
  if (noDoubles && die1 == die2 && die1 == 6 ){
    return 6 + 1; //doble 6
  }
  if (noDoubles && die1 == die2){
    return die1 + die2 + 1; //dobles con true 
  }
  if (die1 != die2){
    return die1 + die2;//numeros distintos
  }
  if (!noDoubles){
    return die1 + die2; //sin dobles
  }
  return 1;
}

public int maxMod5(int a, int b) {
 if (a%5 == b%5 && a>b){
  return b ; // mod a 5 con b menor 
 }
 if (a%5 == b%5 && a<b){
  return a ; // mod a 5 con a menor 
 }
 if (a>b){
   return a ; //a mayor 
 }
 if (a<b){
   return b ; //b mayor 
 }
 if (a == b){ 
  return 0 ;//si son iguales 
}
  return 10 ; //mi nota 
}

public int redTicket(int a, int b, int c) {
  if (a==2 && b==2 && c==2){
    return 10 ; // todos 2 
  }
  if (a==b && b==c ){
    return 5 ; // todos iguales 
  }
  if (a!=b && a!=c){
    return 1;//distintos de a 
  }
  return 0;//sino , 0 
}

public int greenTicket(int a, int b, int c) { 
  if (a==b && b==c ){
    return 20 ; // todos iguales 
  }
  if (a==b || b==c || a==c){
    return 10 ; // dos iguales 
  }
  
  if (a!=b && b!=c){
    return 0;// todos distintos 
  }
  return 000;//random number  
}

public int blueTicket(int a, int b, int c) {
  if (a + b == 10 || b + c == 10 || a + c == 10 ){
    return 10 ; // dos suman 10  
  }
  if (b + c + 10 == a + b  || a + c + 10== a + b ){
    return 5 ; // ab es 10 mas que bc o ac 
  }
  return 0;// cerito al canto   
}

public boolean shareDigit(int a, int b) {
  //comparamos todos los numeros de a y b usando / y % de 10 
  if (a/10 == b/10 || a/10 == b%10 || a%10 == b%10 || a%10 == b/10 ){
    return true ;
  }
  return false;
}

public int sumLimit(int a, int b) {
  // oh por favor , es el ultimo !!! no me lo creo aun. SHOCK!!
  //string de a 
 String as = String.valueOf(a);
 //string de la suma 
 String ss = String.valueOf(a+b);
 //comparamos longitudes 
 if ( as.length() == ss.length() ){
   return a+b;
 }
  return a;
}

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
